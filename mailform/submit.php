<?php
error_reporting(E_ALL & ~E_NOTICE);
require_once "config.php";
if ($enableCaptcha) {
    session_start();
    require "captcha-session.class.php";
    require "captcha.class.php";
}
try
{
    if (count($_POST) == 0) {
        throw new Exception('Form is empty');
    }
    if ($enableCaptcha && !IconCaptcha::validateSubmission($_POST)) {
        throw new Exception('Captcha is not solved: ' . json_decode(IconCaptcha::getErrorMessage())->error);
    }
    $emailText = "";
    foreach ($_POST as $key => $value) {
        if (isset($fieldValidations[$key])) {
            if (!preg_match($fieldValidations[$key] . "iD", $value)) {
                throw new Exception("Field $key is invalid");
            }
        }
        if ($key == $fromField) {
            $from = $value;
        } elseif ($key == $subjectField) {
            $subject .= " - $value";
        }
        if (isset($contentFields[$key])) {
            $emailText .= "$contentFields[$key]: $value<br />";
        }
    }
    $headers = array('Content-Type: text/plain; charset="UTF-8";',
        'From: ' . $from,
        'Reply-To: ' . $from,
        'Return-Path: ' . $from,
    );
    // echo $to . "<br />" . $subject . "<br />" . $emailText . "<br />" . implode("\n", $headers);
    mail($to, $subject, $emailText, implode("\n", $headers));
    $responseArray = array('status' => 'OK');
} catch (Exception $e) {
    $responseArray = array('status' => 'Error', 'message' => $e->getMessage());
}

// print($responseArray["status"]);
// print("<br />" . $responseArray["message"] . "<br />");

// if requested by AJAX request return JSON response
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);
    header('Content-Type: application/json');
    echo $encoded;
}
// else just redirect back or print the status
else {
    if (isset($_GET["return"])) {
        $returnUrl = str_replace("{status}", $responseArray["status"], $_GET["return"]);
        header("location:$returnUrl");
    } else {
        echo $responseArray["status"];
    }
}
