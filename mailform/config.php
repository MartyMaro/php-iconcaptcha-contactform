<?php
$from = "no-reply@example.net";
$to = "youremail@example.net";
$subject = "### FORM SUBMIT REQUEST ###";
$fromField = "email";
$subjectField = "subject";
$contentFields = array(
	"name" => "Name",
	"message" => "Message",
);
$fieldValidations = array(
	"name" => '/^.+$/',
	"email" => '/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/',
	"subject" => '/^.+$/',
	"message" => '/^(?:.|[\r\n])+$/',
);
$enableCaptcha = true;
?>