# IconCaptcha - Contact Form #
A simplified PHP driven example of a secured contact form.

## Information ##

The project does not come with any pretty stylesheet, as you should build the form for your website in any case, thus you should have a proper stylesheet, already. Only the [IconCaptcha](https://www.fabianwennink.nl/projects/IconCaptcha/), that was made by [Fabian Wennink](https://www.fabianwennink.nl) requires you to include several assets that can be found in the assets folder. 

Build your html contact form like in [index.php](index.php) and configure your selected fields in the [config.php](mailform/config.php). The server-sided submitting script will redirect to the page that you put in the `return` GET parameter on submit. It will replace `{status}` with `OK` or `Error` depending if anything like field validation or captcha validation fails. This can be used to display any errors to the user, however the demo does not include this, since this can be presented, very individually. 

## Demo ##

You can check a very bare-bone live demo [here](http://marty.epizy.com/demos/php-iconcaptcha-contactform/). The mail function won't be executed, though. Please take a look at the URL after submitting the form.