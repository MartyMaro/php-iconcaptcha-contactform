<?php
session_start();
require "mailform/captcha-session.class.php";
require "mailform/captcha.class.php";
IconCaptcha::setIconsFolderPath("../assets/icons/");
IconCaptcha::setIconNoiseEnabled(true);
?>
<html>

<head>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

	<link href="assets/css/icon-captcha.min.css" rel="stylesheet" type="text/css">
	<script src="assets/js/icon-captcha.min.js" type="text/javascript"></script>
</head>

<body>

	<form method="post" action="mailform/submit.php?return=../?mfs={status}" role="form">
		<input name="name" type="text" required placeholder="Name*" />
		<br />
		<input name="email" type="email" required placeholder="Email*" />
		<br />
		<input name="subject" type="text" required placeholder="Subject*" />
		<br />
		<textarea name="message" required placeholder="Message*"></textarea>
		<br />
		<div class="captcha-holder"></div>
		<input type="submit" value="SEND" />
		*Required fields
		<script async type="text/javascript">
			$(window).ready(function() {
				$('.captcha-holder').iconCaptcha({
					captchaTheme: "light", // Select the theme(s) of the Captcha(s). Available: light, dark
					captchaFontFamily: "", // Change the font family of the captcha. Leaving it blank will add the default font to the end of the <body> tag.
					captchaClickDelay: 500, // The delay during which the user can't select an image.
					captchaHoverDetection: true, // Enable or disable the cursor hover detection.
					enableLoadingAnimation: true, // Enable of disable the fake loading animation. Doesn't actually do anything other than look nice.
					loadingAnimationDelay: 2500, // How long the fake loading animation should play.
					showCredits: "hide", // Show, hide or disable the credits element. Valid values: 'show', 'hide', 'disabled' (please leave it enabled).
					requestIconsDelay: 7500, // How long should the script wait before requesting the hashes and icons? (to prevent a high(er) CPU usage during a DDoS attack)
					captchaAjaxFile: "mailform/captcha-request.php", // The path to the Captcha validation file.
					captchaMessages: { // You can put whatever message you want in the captcha.
						header: "Select the image that does not belong in the row*",
						correct: {
							top: "Great!",
							bottom: "You do not appear to be a robot."
						},
						incorrect: {
							top: "Oops!",
							bottom: "You've selected the wrong image."
						}
					}
				})
				// .bind('init.iconCaptcha', function(e, id) { // You can bind to custom events, in case you want to execute some custom code.
				// 		console.log('Event: Captcha initialized', id);
				// }).bind('selected.iconCaptcha', function(e, id) {
				// 		console.log('Event: Icon selected', id);
				// }).bind('refreshed.iconCaptcha', function(e, id) {
				// 		console.log('Event: Captcha refreshed', id);
				// }).bind('success.iconCaptcha', function(e, id) {
				// 		console.log('Event: Correct input', id);
				// }).bind('error.iconCaptcha', function(e, id) {
				// 		console.log('Event: Wrong input', id);
				// });
			});
		</script>
	</form>

</body>

</html>
